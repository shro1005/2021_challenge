2021 공부 챌린지  
  
1. CS fundamental  
  
    OS  :  프로세스, 쓰레드, 임계영역 (critical section) -> 뮤텍스, 세마포어  
    DB (트랜잭션)  
    네트워크 / 통신 (HTTPS)  
    JAVA (OOP, java 8)  
    디자인 패턴
    NIO : 개념 (비동기랑 논블로킹 차이)
    자료구조    
    알고리즘 (시간복잡도, 부동소수점 처리방법 등)  
          
2. Spring 진영, java 학습 (행복워치 리팩토링 등)  
    
    gc 커스터마이징 
    java 8 이전 (future, reflect, optional ...)
    java 8 (Stream, lambda(functional programming))  
    Spring에서 AOP, Transaction  
    Spring Batch 심화 ( + quartz 를 사용한 클러스터링 배치 처리 도입 )  
    Spring Security (Oauth2 본격 학습, Session, 쿠키, 토큰 관리)  
    Spring JPA - 심화 학습 및 사용 (행복워치 Entity간 연관관계 재설정 등)  
    Spring WebFlux 를 사용한 간단한 토이 프로젝트 진행   
          
    ( + query dsl 학습 )  

        
3. Node JS 학습 (진핸중인 앱개발 등)  
  
    현재 진행중인 앱 출시  
    Promise, Async Await 제대로 이해  
    scope, closure
    AWS lambda 및 Serverless Framework 숙련도 향상 (layer, 모듈화, 람다에서 db connection pool 사용방법 등)  


4. Front 학습 (Vue || React)  

    대세 프론트 언어 학습 및 javascript es6 ~ es8 까지 문법 제대로 학습  


5. 오픈소스 솔루션 경험해보기  

    kafka, zookeeper  
    Docker, k8s, 프로메테우스, 그라파나
    
